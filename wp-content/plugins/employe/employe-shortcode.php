<?php
//Fonction principal et globale réutilisée par les autres fonctions - Ca fait le café mais pas trop
function display_custom_post($atts) {
	
	//Initialise le nombre de page
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;    
	
	//Attribut par défaut
	$atts = shortcode_atts    (
			array(
				'post_type' => 'post', // !!! Ca affiche tous les post !!!
				'posts_per_page' => 5,
				'orderby' 	=> 'post_title',
				'order' 	=> 'ASC',
				'paged'		=> $paged,
				'champs' => array(),
				'p'      =>'',
				'titre_champs' => array(),
				 ),
				$atts
			);
	//Classique, on lance la requête
	$custom_post = new WP_Query( $atts );    
	//Initilisation de la variable de sortie
    $output = '';
	//On regarde si le WP_Query nous a renvoyé un résultat
	if ($custom_post->have_posts()) {
		//La loop qui se répète tant qu'il y a des posts - L'itération est intégrée (curseur)
		$output .= '<div class="flex-container">';
		while ( $custom_post->have_posts() ) : $custom_post->the_post();
			//Construction de notre variable de sortie
			$output .= '<div id="member-container">';
			$output .= '<div class="member">';
			$output .= '<article>';
			$output .= '<h1>'.get_the_title(get_the_ID()).'</h1>';
			//La photo de la série
			$output .= '<div>'.get_the_post_thumbnail(get_the_ID(),'thumbnail').'</div>';
			$output .= '<p>'.get_the_excerpt(get_the_ID()).'</p>';
			$output .= '<div class="custom-post-content d-flex flex-column">';
			
			if (count($atts['champs'])>0){
				foreach ($atts['champs'] as $key_champ => $champ){
					$output .= '<span>';
					$output .= $atts['titre_champs'][$key_champ];
					$output .= get_the_term_list(get_the_ID(), $champ);
					$output .= '</span>';					
				}
			}

			$output .= '</div>';
			$output .= '</article>';
			$output .= '<div class="soc-icons">';
			$output .=	'<a href="https://twitter.com">&#xf099;</a>';
			$output .= '<a href="https://linkedin.com">&#xf0e1;</a>';
			$output .= '<a href="https://github.com/">&#xf09b;</a>';
			$output .= '<a href="https://plus.google.com">&#xf0d5;</a>';
			$output .= '</div>';

			$output .= '</div>';
			$output .= '</div>';
		endwhile;
		$output .= '</div>';
		//Si il y a lieu de faire plusieurs page de navigation, alors on cré la navigation
		if($atts['paged']!=""){
			//Page maximimum nombre au pifomètre
			$big = 999999999; 
			//Intégration de la navigation dans la variable de sortie
			$output .= paginate_links( array(
								'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
								'format' => '?paged=%#%',
								'current' => max( 1, get_query_var('paged') ),
								'total' => $custom_post->max_num_pages
								) );  
		}  
		//Réinitialisation de notre curseur
		wp_reset_postdata(); 

	}else{
		//En cas de retour vide de notre WP_Query
		$output .= '<div class="custom-post-content">';
		$output .= 'Aucun résultat pour les posts de type '.$atts['post_type'].'.';
		$output .= '</div>';
	} 
	 
	return $output;  
}

//add_shortcode( 'display_custom_post' , 'display_custom_post' );

///////////////////


function display_seriestv($atts) {
	$champs = array('secteurs', 'batiments', 'roles');
	$titre_champs = array('Secteur : ','batiment: ','Rôle : ');
	//Construction du paramètre pour afficher les séries tv
	$atts = shortcode_atts(array(
		'post_type' => 'seriestv', 
		'champs'=>$champs, 
		'p'=>'',
		'titre_champs'=>$titre_champs),$atts); 
	//On envoie ce paramètre dans notre fonction principal et on retourne directement le résultat
	return display_custom_post($atts);
}
//Création du shortcode [seriestv] par rapport à la fonction display_seriestv
add_shortcode( 'employes' , 'display_seriestv' );