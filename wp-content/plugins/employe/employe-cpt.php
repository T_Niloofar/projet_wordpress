<?php
/*
* On utilise une fonction pour créer notre custom post type 'Séries TV'
*/

function wpm_custom_post_type_employe() {
	register_widget(EmployeInformation::class);
	// On rentre les différentes dénominations de notre custom post type qui seront affichées dans l'administration
	$labels = array(
		// Le nom au pluriel
		'name'                => _x( 'Les employes', 'Post Type General Name'),
		// Le nom au singulier
		'singular_name'       => _x( 'Employe', 'Post Type Singular Name'),
		// Le libellé affiché dans le menu
		'menu_name'           => __( 'Les employes'),
		// Les différents libellés de l'administration
		'all_items'           => __( 'Toutes les employés'),
		'view_item'           => __( 'Voir les employés'),
		'add_new_item'        => __( 'Ajouter un employé'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer employé'),
		'update_item'         => __( 'Modifier employé'),
		'search_items'        => __( 'Rechercher un employé'),
		'not_found'           => __( 'Non trouvée'),
		'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
	);
	
	// On peut définir ici d'autres options pour notre custom post type
	
	$args = array(
		'label'               => __( 'les employés'),
		'description'         => __( 'Tous sur les employés'),
		'labels'              => $labels,
		'menu_icon'           => 'dashicons-businessperson',
		// On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		/* 
		* Différentes options supplémentaires
		*/	
		'show_in_rest' => true,
		'hierarchical'        => false,
		'public'              => true,
		'has_archive'         => true,
		'rewrite'			  => array( 'slug' => 'series-tv'),

	);
	
	// On enregistre notre custom post type qu'on nomme ici "serietv" et ses arguments
	register_post_type( 'seriestv', $args );

}

add_action( 'init', 'wpm_custom_post_type_employe', 0 );
 
add_action( 'init', 'wpm_add_taxonomies2', 0 );

//On crée 3 taxonomies personnalisées: Année, Réalisateurs et Catégories de série.

function wpm_add_taxonomies2() {
	
	// Taxonomie Secteur

	// On déclare ici les différentes dénominations de notre taxonomie qui seront affichées et utilisées dans l'administration de WordPress
	$labels_secteur = array(
		'name'              			=> _x( 'Secteurs', 'taxonomy general name'),
		'singular_name'     			=> _x( 'Secteur', 'taxonomy singular name'),
		'search_items'      			=> __( 'Chercher un secteur'),
		'all_items'        				=> __( 'Toutes les secteurs'),
		'edit_item'         			=> __( 'Editer le secteur'),
		'update_item'       			=> __( 'Mettre à jour le secteur'),
		'add_new_item'     				=> __( 'Ajouter une nouvelle secteur'),
		'separate_items_with_commas'	=> __( 'Séparer les réalisateurs avec une virgule'),
		'menu_name'         => __( 'Secteur'),
	);

	$args_secteur = array(
	// Si 'hierarchical' est défini à false, notre taxonomie se comportera comme une étiquette standard
		'hierarchical'      => false,
		'labels'            => $labels_secteur,
		'show_ui'           => true,
		'show_in_rest' => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'secteurs' ),
	);

	register_taxonomy( 'secteurs', 'seriestv', $args_secteur );

	// Taxonomie Batiment
	
	$labels_batiments = array(
		'name'                       => _x( 'Batiments', 'taxonomy general name'),
		'singular_name'              => _x( 'Batiment', 'taxonomy singular name'),
		'search_items'               => __( 'Rechercher un Batiment'),
		'popular_items'              => __( 'Batiments populaires'),
		'all_items'                  => __( 'Tous les Batiments'),
		'edit_item'                  => __( 'Editer un Batiment'),
		'update_item'                => __( 'Mettre à jour un Batiment'),
		'add_new_item'               => __( 'Ajouter un nouveau Batiment'),
		'new_item_name'              => __( 'Nom du nouveau Batiment'),
		'separate_items_with_commas' => __( 'Séparer les Batiments avec une virgule'),
		'add_or_remove_items'        => __( 'Ajouter ou supprimer un Batiment'),
		'choose_from_most_used'      => __( 'Choisir parmi les plus utilisés'),
		'not_found'                  => __( 'Pas de Batiments trouvés'),
		'menu_name'                  => __( 'Batiments'),
	);

	$args_batiments = array(
		'hierarchical'          => false,
		'labels'                => $labels_batiments,
		'show_ui'               => true,
		'show_in_rest'			=> true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'batiments' ),
	);

	register_taxonomy( 'batiments', 'seriestv', $args_batiments );
	
	
	// Taxonomie Role
	
	$labels_roles = array(
		'name'                       => _x( 'Roles', 'taxonomy general name'),
		'singular_name'              => _x( 'Role', 'taxonomy singular name'),
		'search_items'               => __( 'Rechercher un role'),
		'popular_items'              => __( 'Roles populaires'),
		'all_items'                  => __( 'Tous les roles'),
		'edit_item'                  => __( 'Editer un role'),
		'update_item'                => __( 'Mettre à jour un role'),
		'add_new_item'               => __( 'Ajouter un nouveau role'),
		'new_item_name'              => __( 'Nom du nouveau role'),
		'separate_items_with_commas' => __( 'Séparer les roles avec une virgule'),
		'add_or_remove_items'        => __( 'Ajouter ou supprimer un role'),
		'choose_from_most_used'      => __( 'Choisir parmi les plus utilisés'),
		'not_found'                  => __( 'Pas de roles trouvés'),
		'menu_name'                  => __( 'Roles'),
	);

	$args_roles = array(
		'hierarchical'          => false,
		'labels'                => $labels_roles,
		'show_ui'               => true,
		'show_in_rest'			=> true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'roles' ),
	);

	register_taxonomy( 'roles', 'seriestv', $args_roles );



}

