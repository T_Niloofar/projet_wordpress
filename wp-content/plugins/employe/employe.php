<?php
/**
 * @package employe
 */
/*
Plugin Name: niloofar
Plugin URI: https://
Description: Ajoute un nouveau post de type serietv et permet de les gérer de différentes manières
Version: 5.0.0
Author: Niloofar
Author URI: https://
License: GPLv2 or later
Text Domain: employe
*/

//Pour être sûr qu'aucune infos ne sont accessibles en étant appelées directement
if ( !function_exists( 'add_action' ) ) {
	echo 'Tu m\'as appelé ? Ben non, je suis occupé.';
	exit;
}

//Définir la constante globale menant au répertoir du plugin
define( 'COLLABO__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

//Utile si on veut lancer des fonctions à l'activation ou désactivation du plugin
/*register_activation_hook( __FILE__, array( 'Série', 'plugin_activation' ) );
register_deactivation_hook( __FILE__, array( 'Série', 'plugin_deactivation' ) );*/

//Pour ajouter un CSS au plugin et BOOTSTRAP
function add_scripts_styles() {

	
	//CSS
	wp_register_style('main-employe-style', plugins_url( 'style.css', __FILE__ ), array(), true);
	wp_enqueue_style('main-employe-style');

	//BOOTSTRAP
	wp_register_style('bootstrap-style', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css', array(), '');
	wp_enqueue_style('bootstrap-style');

	wp_register_script('bootstrap-js1', 'https://code.jquery.com/jquery-3.5.1.slim.min.js', array( 'jquery' ),'',true);
	wp_enqueue_script( 'bootstrap-js1');

	wp_register_script('bootstrap-js2', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js', array( 'jquery' ),'',true);
	wp_enqueue_script( 'bootstrap-js2');

	wp_register_script('bootstrap-js3', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js', array( 'jquery' ),'',true);
	wp_enqueue_script( 'bootstrap-js3');
	
}
//Lance la fonction ci-dessus au hook wp_enqueue_scripts
add_action( 'wp_enqueue_scripts', 'add_scripts_styles' );



//On appel nos autres fichiers php
require_once( COLLABO__PLUGIN_DIR . 'employe-cpt.php' );
require_once( COLLABO__PLUGIN_DIR . 'employe-shortcode.php' );
require_once( COLLABO__PLUGIN_DIR . 'troisEmploye.php' );
require_once( COLLABO__PLUGIN_DIR . 'employeWidget.php' );
require_once( COLLABO__PLUGIN_DIR . 'employeMetaBox.php' );
