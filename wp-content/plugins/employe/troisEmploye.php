<?php
//Fonction principal et globale réutilisée par les autres fonctions - Ca fait le café mais pas trop
function display_custom_post3($atts) {
	
	//Initialise le nombre de page
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;    
	
	//Attribut par défaut
	$atts = shortcode_atts    (
			array(
				'post_type' => 'post', // !!! Ca affiche tous les post !!!
				'posts_per_page' => 3,
				'orderby' 	=> 'p',
				'order' 	=> 'DESC',
				'paged'		=> $paged,
				'champs' => array(),
				'p'      =>'',
				'titre_champs' => array(),
				 ),
				$atts
			);
	//Classique, on lance la requête
	$custom_post = new WP_Query( $atts );    
	//Initilisation de la variable de sortie
    $output = '';
	//On regarde si le WP_Query nous a renvoyé un résultat
	if ($custom_post->have_posts()) {
		$output .= '<div class="container">';
		//La loop qui se répète tant qu'il y a des posts - L'itération est intégrée (curseur)
		while ( $custom_post->have_posts() ) : $custom_post->the_post();
			//Construction de notre variable de sortie
			
			$output .= '<article class="troisEmploye">';
			$output .= '<p>'.get_the_excerpt(get_the_ID()).'</p>';
			$output .= '<div class="custom-post-content d-flex flex-column">';			
			if (count($atts['champs'])>0){
				foreach ($atts['champs'] as $key_champ => $champ){
					$output .= '<span>';
					$output .= $atts['titre_champs'][$key_champ];
					$output .= get_the_term_list(get_the_ID(), $champ);
					$output .= '</span>';					
				}
			}

			//La photo de la série
			$output .= '<div>'.get_the_post_thumbnail(get_the_ID(),'medium').'</div>';
			$output .= '</div>';
			$output .= '</article>';
			

		endwhile;
		$output .= '</div>';
		
		//Réinitialisation de notre curseur
		wp_reset_postdata(); 

	}else{
		//En cas de retour vide de notre WP_Query
		$output .= '<div class="custom-post-content">';
		$output .= 'Aucun résultat pour les posts de type '.$atts['post_type'].'.';
		$output .= '</div>';
	} 
	 
	return $output;  
}

//add_shortcode( 'display_custom_post' , 'display_custom_post' );

///////////////////


function display_seriestv2($atts) {
	$champs = array('secteurs', 'batiments', 'roles');
	$titre_champs = array('Secteur : ','batiment: ','Rôle : ');
	//Construction du paramètre pour afficher les séries tv
	$atts = shortcode_atts(array(
		'post_type' => 'seriestv', 
		'champs'=>$champs, 
		'p'=>'',
		'titre_champs'=>$titre_champs),$atts); 
	//On envoie ce paramètre dans notre fonction principal et on retourne directement le résultat
	return display_custom_post3($atts);
}
//Création du shortcode [seriestv] par rapport à la fonction display_seriestv
add_shortcode( 'troisEmploye' , 'display_seriestv2' );